

class Day1:
    def __init__(self, filename='input/1.1.txt'):
        f = open(filename)
        self.movelist = ''.join(f.readlines())

    def movegenerator(self):
        n = 0
        while n < len(self.movelist):
            yield self.movelist[n]
            n += 1

    def part1(self):
        floor = 0
        for m in self.movegenerator():
            if m is '(':
                floor += 1
            if m is ')':
                floor -= 1
        return floor

    def part2(self):
        floor = 0
        step = 1
        for m in self.movegenerator():
            if m is '(':
                floor += 1
            if m is ')':
                floor -= 1
            if floor < 0:
                return step
            step += 1
