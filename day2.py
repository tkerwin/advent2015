

class Day2:
    def __init__(self, filename='input/2.1.txt'):
        f = open(filename)
        boxes = f.readlines()
        self.boxdims = []
        for box in boxes:
            textdims = box.split('x')
            self.boxdims.append([int(x) for x in textdims])

    def sidesizes(b):
        return (b[0]*b[1], b[0]*b[2], b[1]*b[2])

    def part1(self):
        total = 0
        for d in self.boxdims:
            sides = Day2.sidesizes(d)
            total += 2 * sum(sides) + min(sides)
        return total

    def part2(self):
        total = 0
        for d in self.boxdims:
            # length around the box
            total += 2*sum(d) - 2*max(d)
            # Length for bow
            total += d[0] * d[1] * d[2]
        return total
