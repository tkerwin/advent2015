

class Day3:
    def __init__(self, filename='input/3.1.txt'):
        f = open(filename)
        self.moves = ''.join(f.readlines())

    def locations(moves):
        pos = (0, 0)
        locs = [pos,]
        for m in moves:
            if m is '^':
                newpos = (pos[0], pos[1]+1)
            elif m is 'v':
                newpos = (pos[0], pos[1]-1)
            elif m is '<':
                newpos = (pos[0] - 1, pos[1])
            elif m is '>':
                newpos = (pos[0] + 1, pos[1])
            locs.append(newpos)
            pos = newpos
        return locs

    def part1(self):
        locs = Day3.locations(self.moves)
        s = set(locs)
        return len(s)

    def part2(self):
        realsanta = Day3.locations(self.moves[::2])
        robosanta = Day3.locations(self.moves[1::2])
        s = set(realsanta).union(robosanta)
        return len(s)
