import hashlib

class Day4:
    def __init__(self, prefix='yzbqklnj'):
        self.prefix = prefix

    def part1(self):
        n = 0
        while True:
            h = hashlib.md5(bytes(self.prefix + str(n), 'ascii'))
            if h.hexdigest()[0:5] == '00000':
                return n
            n += 1

    def part2(self):
        n = 0
        while True:
            h = hashlib.md5(bytes(self.prefix + str(n), 'ascii'))
            if h.hexdigest()[0:6] == '000000':
                return n
            n += 1
