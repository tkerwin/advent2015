import re

class Day5:
    def __init__(self, filename='input/5.1.txt'):
        f = open(filename)
        self.strings = f.readlines()

    def hasBanned(s, bannedlist=['ab', 'cd', 'pq', 'xy']):
        # make a regex that has 'ored' all the strings in the bannedlist
        rx = re.compile('|'.join(['(' + x + ')' for x in bannedlist]))
        return rx.search(s) is not None

    def hasThreeVowels(s):
        rx = re.compile('[a|e|i|o|u]')
        if len(rx.findall(s)) < 3:
            return False
        else:
            return True

    def hasDoubleLetter(s):
        rx = re.compile(r'([a-zA-Z])\1')
        return rx.search(s) is not None

    def part1(self):
        nice_count = 0
        for s in self.strings:
            if Day5.hasThreeVowels(s) and Day5.hasDoubleLetter(s) and not Day5.hasBanned(s):
                nice_count += 1
        return nice_count

    def pairTwice(s):
        rx = re.compile(r'(\w\w).*\1')
        return rx.search(s) is not None

    def oneLetterGap(s):
        rx = re.compile(r'(\w)\w\1')
        return rx.search(s) is not None

    def part2(self):
        nice_count = 0
        for s in self.strings:
            if Day5.pairTwice(s) and Day5.oneLetterGap(s):
                nice_count += 1
        return nice_count
