import re
from collections import namedtuple

Op = namedtuple('op', ['operation', 'operand1', 'operand2'])

def makeop(operation, operand1, operand2):
    try:
        operand1 = int(operand1)
    except ValueError:
        pass

    try:
        operand2 = int(operand2)
    except ValueError:
        pass

    return Op(operation, operand1, operand2)

class Day7:
    def __init__(self, filename='input/7.1.txt'):
        f = open(filename)
        definitions = f.readlines()
        self.rules = {}
        for d in definitions:
            tokens = d.strip().split(' ')
            dest = tokens[-1]
            if len(tokens) == 3:
                operation = 'ASSIGN'
                operand1 = tokens[0]
                operand2 = 0
            elif len(tokens) == 4:
                operation = tokens[0]
                operand1 = tokens[1]
                operand2 = 0
            elif len(tokens) == 5:
                operation = tokens[1]
                operand1 = tokens[0]
                operand2 = tokens[2]
            else:
                continue
            self.rules[dest] = makeop(operation, operand1, operand2)
        self.known_values = {}

    def tokenizer(s):
        s.split(' ')

    def resolve(self, identifier):
        if type(identifier) == int:
            return identifier
        elif identifier in self.known_values:
            return self.known_values[identifier]

        op = self.rules[identifier]
        op1val = self.resolve(op.operand1)
        op2val = self.resolve(op.operand2)

        if op.operation == 'AND':
            result = op1val & op2val
        elif op.operation == 'OR':
            result = op1val | op2val
        elif op.operation == 'RSHIFT':
            result = op1val >> op2val
        elif op.operation == 'LSHIFT':
            result = op1val << op2val
        elif op.operation == 'NOT':
            result = ~op1val
        elif op.operation == 'ASSIGN':
            result = op1val

        self.known_values[identifier] = result
        return result

def part1():
    d = Day7()
    return d.resolve('a')

def part2():
    d = Day7()
    b = d.resolve('a')
    d.known_values = {'b': b}
    return d.resolve('a')
